class Machine {

	public void start() {

		System.out.println("Starting machine...");
	}

}

// zeby zaimplementowac ten interface mozna stworzyc klase
// and say it implements plant i wtedy musze dodac metode bo inaczej sie nie da
// ale mozna tez uzyc anonimowej klasy
interface Plant {
	public void grow();

}

public class App {

	public static void main(String[] args) {

		
		// This is equivalent to creating a class that "extends"
        // Machine and overrides the start method.
		// i teraz jak chce zrobic override tej klasy machine
		// sposob 1 - to tworze klase car np i idaje tam metode
		// sposob 2 - lub robie to co ponizej czyli otwieram {}
		// dobry override na dziecej klasie maszyny
		// i to jet anonimowa klasa!!!
		// nowa jedna klasa troche inna od istniejacej do tego lzuzy anonimowa
		// klasa
		Machine machine1 = new Machine() {
			@Override
			public void start() {

				System.out.println("Camera snapping");

			}
		};
		machine1.start();

		
		// This is equivalent to creating a class that "implements"
        // the Plant interface
		// uzywam anonimowej do zaimplementowania interface'u
		// instaniate = stworzyc obiekt z klasy interfaceu
		Plant plant1 = new Plant() {
			@Override
			public void grow() {

				System.out.println("Plant growing");
			}

		};
		// implementuje interface
		plant1.grow();

	}
}